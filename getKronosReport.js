var request = require("request");
var currToken = require('./getToken');
var fs = require("fs")


//Get current active token.
currToken.activeToken(function(er, token) {
  if (er) {
    return console.log(er);
  };
  
//nconsole.log(token)
  var options = { method: 'POST',
  url: 'https://secure5.saashr.com/ta/rest/v1/report/saved/89147853',
  qs: { 'company:shortname': 'HP20B' },
  headers: 
   { 'cache-control': 'no-cache',
     Connection: 'keep-alive',
     'accept-encoding': 'gzip, deflate',
     Host: 'secure5.saashr.com',
     'Cache-Control': 'no-cache',
     Authentication: 'Bearer '+token,
     Accept: 'text/csv',
     'Content-Type': 'application/json' },
  body: 
   { company: { short_name: 'HP20B' },
     page: { number: 0, size: 15 },
     fields: 
      [ 'RoutingNum',
        'Active',
        'AccountType',
        'EmplStatusName',
        'Created',
        'CustomColumn0',
        'CustomColumn1',
        'CustomColumn2',
        'CustomColumn19',
        'CustomColumn20',
        'CustomColumn23',
        'EmplLastName',
        'EmplFirstName',
        'EmplNickname',
        'EmplTitle',
        'EmplPrimaryAddrAddress1',
        'EmplPrimaryAddrAddress2',
        'CustomColumn3',
        'EmplPrimaryAddrCity',
        'EmplPrimaryAddrStateId',
        'CustomColumn21',
        'EmplPrimaryAddrZip',
        'EmplDateBirthday',
        'EmplSS',
        'CustomColumn4',
        'CustomColumn5',
        'CustomColumn6',
        'EmplPrimaryEmail',
        "CustomColumn8",
        "EmplWorkPhone",
        "CustomColumn9",
        'CustomColumn7',
        "EmplPayGrade",
        "EmplTrainingProfile",
        "CustomColumn10",
        "CustomColumn18",
        "CustomColumn24",
        "CustomColumn11",
        "CustomColumn12",
        "CustomColumn13",
        "CustomColumn14",
        "EmplUnemploymentState",
        "EmplAccountId",
        "EmplAltAddrAddress1",
        "EmplAltAddrAddress2",
        "EmplAltAddrCity",
        "EmplAltAddrStateId",
        "CustomColumn22",
        "EmplAltAddrZip",
        "CustomColumn15",
        "CustomColumn16",
        "CustomColumn17",
        'EmplCustom8' ],
     filters: 
      [ { field_name: 'EmplCustom8', operator: 'is_null' },
        { field_name: 'RoutingNum', operator: '=', values: ["072413256"] },
        { field_name: 'Active', operator: 'is', values: ["Yes"] },
        { field_name: 'EmplStatusName',
          operator: '!=',
          values: [ 'Terminated' ] } ] },
  json: true };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  
  //console.log(body)
  
  //remove double quotes from file. 
  var table = body.replace(/"/g, '');
  
  //Remove Header
  var noHeaderTable = table.substring(table.indexOf("\n") + 1) 
   
 //Writes original table to file
  fs.writeFile("orginal.csv", body, function(err){
    if (err) return console.error(err)
   })
   //Grabs the current report in Kronos and then saves it as a text or csv file. 
   fs.writeFile("cdata.csv", noHeaderTable, function(err){
     if (err) return console.error(err)
    })

  })

});


