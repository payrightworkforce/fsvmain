var request = require("request");
var currToken = require('./getToken')

exports.cancelPreNote = async function cancelPreNote(dd_id, abaNum){ 
    currToken.activeToken(function(er, token) {
        if (er) {
        return console.log(er);
        };   

       
        var options = { method: 'PUT',
        url: 'https://secure.saashr.com/ta/rest/v2/companies/83927296/direct-deposits/actions/mass-edit',
        headers: 
        { 'cache-control': 'no-cache',
            'Accept-Encoding': 'gzip, deflate',
            Host: 'secure.saashr.com',
            Authentication: 'Bearer '+token,
            Accept: 'application/json',
            'Content-Type': 'application/json' },
        body: {
            "direct_deposit_ids": [
                  dd_id
            ],
            "aba": abaNum,
            "pre_note_status": "PRE_NOTE_STATUS_CANCELLED"
          },
        json: true };

        request(options, function (error, response, bodyRes) {
        if (error) throw new Error(error);

        //console.log(bodyRes);
        console.log("Pre-Note Cancelation Status: "+response.body.status)
        });

    })
}
