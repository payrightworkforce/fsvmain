var fs = require("fs");
var request = require("request");
var currToken = require('./getToken');
var getImportStatus = require("./importStatus")


exports.importFSV = function importFSV(file){
    
    //Read File
    var reader = fs.readFileSync(file).toString()
    
    //Remove the last empty row 
    var reader2 = reader.substring(reader.lastIndexOf("\n"),0)

    currToken.activeToken(function(er, token) {
        if (er) {
        return console.log(er);
        }; 
    
    //Import the file into Kronos.      
        var options = { method: 'POST',
        url: 'https://secure.saashr.com/ta/rest/v1/import/100',
        headers: 
        {    Connection: 'keep-alive',
            'accept-encoding': 'gzip, deflate',
            Host: 'secure.saashr.com',
            'Cache-Control': 'no-cache',
            Authentication: 'Bearer '+token,
            Accept: 'application/json',
            'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' },
        formData: 
        { file: 
            { value: reader2,
                options: 
                { filename: file,
                contentType: null } } } };
        
        request(options, function (error, response, body) {
        if (error) throw new Error(error);
        
        //console.log(body);
        //console.log(reader2)
        
        //Get jobId 
        var jobId = response.headers.location.replace("/import/status/","")
        getImportStatus.getImportStatus(jobId)
        })
    })
}