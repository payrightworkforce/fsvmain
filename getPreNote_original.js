var request = require("request");
var currToken = require('./getToken');
var fs = require('fs');
var jsonexport = require('jsonexport');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
var importPreNote = require("./preNoteImport")


exports.getPreNote = function getPreNote(chEmployeeId){
currToken.activeToken(function(er, token) {
    if (er) {
      return console.log(er);
    };
  
    var options = { method: 'GET',
    url: 'https://secure.saashr.com/ta/rest/v2/companies/83927296/employees/'+chEmployeeId, 
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authentication': 'Bearer '+token
    }  };

    request(options, function (error, response, body) {
    if (error) throw new Error(error);

    //console.log(JSON.parse(body));

    //FSV import function: parse JSON Body, creates excel file, and imports employee's data into Kronos. 
    async function PreNoteimport(){
        var jsonFile = JSON.parse(body)
        //console.log(jsonFile)
        //console.log("This is the body "+body)
        
   
            var employee_id = jsonFile.employee_id
            var username = jsonFile.username
            

            var options = { method: 'GET',
            url: 'https://secure.saashr.com/ta/rest/v2/companies/83927296/employees/'+chEmployeeId+'/direct-deposits',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authentication': 'Bearer '+token
            }};

          request(options, function (error2, response2, body2) {
            if (error) throw new Error(error);

            //console.log(body);
            var jsonFile2 = JSON.parse(body2)
            //console.log(body2)

            var pre_note_status = "Cancelled"
            var sequence = jsonFile2.deposits[0].sequence
            var frequency1 = jsonFile2.deposits[0].payroll_frequency.type.replace("_","")
            var frequencyEvery = "EveryPay" 
            var accountType = jsonFile2.deposits[0].account_type
            var aba = jsonFile2.deposits[0].routing_number
            var calc_method = jsonFile2.deposits[0].calc_method.display_name
            var start_date = jsonFile2.deposits[0].active_from
            var account_number = jsonFile2.deposits[0].account_number
            var file = "preNoteImports/"+chEmployeeId+"_"+'import.csv'

            const csvWriter = createCsvWriter({
              path: file,
              header: [
                {id: 'username', title: 'Username'},
                {id: 'employee_id', title: 'Employee Id'},
                {id: 'pre_note_status', title: 'Pre-Note Status'},
                {id: 'sequence', title: 'Sequence'},
                {id: 'frequency', title: 'Frequency'},
                {id: 'accountType', title: 'Account Type'},
                {id: 'aba', title: 'ABA'},
                {id: 'calc_method', title: 'Calculation Method'},
                {id: 'start_date', title: 'Start Date'},
                {id: 'account_number', title: 'Account Number'},

              ]
            });

            const data = [
              {
                username: jsonFile.username,
                employee_id: jsonFile.employee_id,
                pre_note_status: "Cancelled", 
                sequence : jsonFile2.deposits[0].sequence,
                frequency : frequencyEvery,
                accountType : jsonFile2.deposits[0].account_type,
                aba : jsonFile2.deposits[0].routing_number,
                calc_method: jsonFile2.deposits[0].calc_method.display_name,
                start_date : jsonFile2.deposits[0].active_from,
                account_number: jsonFile2.deposits[0].account_number
              }];

                csvWriter
                .writeRecords(data)
                .then(()=> importPreNote.importPreNote(file), 
                console.log('The CSV file was written successfully'),
                );   


            //cancelPreNote.cancelPreNote(dd_id, abaNum)
            //console.log("Here is the response from your put request "+response.statusMessage)
          })

           
    
          Promise.all([request(),csvWriter, data])

    }
    
    PreNoteimport(); 

        });
    })
}