var request = require("request");
var currToken = require('./getToken');

//Get Import Status
exports.getImportStatus = function getImportStatus(jobId) {
  currToken.activeToken(function(er, token) {
    if (er) {
      return console.log(er);
    }; 

    var options = { method: 'GET',
      url: 'https://secure.saashr.com/ta/rest/v1/import/status/'+jobId,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authentication': 'Bearer '+token
    } };
    

    request(options, function (error, response, body) {
      if (error) throw new Error(error);

      console.log("Job ID is " +jobId+ " and response is: "+response.statusCode);
    })
  })
}