var request = require("request"); 
var fs = require("fs");
const nodemailer = require("nodemailer");
var currTable = require("./getKronosReport") 
var request = require("request"); 
var sendMessage = require("./mail")
var successMail = require("./successMail")
var sendPushRequest = require("./putRequest")
var moment = require("moment");
var currToken = require('./getToken')
var errorMail = "<b>"+"Check your errors.html and cdata.csv for more information"+"</b>"
const log = require('simple-node-logger').createSimpleLogger('error_log/errors2.html');
var getEmpRequest = require("./getEmployee.js")
var getPreNote = require("./getPreNote")
var cancelPreNote = require("./cancelPreNote")
var PNImport = require("./PNImport")
const pre_note_mail = require("./error_emails/pre_note_error_email")



fs.readFileSync("cdata.csv").toString().split("\n").forEach(function(cdata, index, arr) {

  var options = { 
    method: 'POST',
    url: 'https://portal.paychekplus.com/fsvremote/services/fsvremote',
    qs: { WSDL: '' },  
    headers: 
    { 'cache-control': 'no-cache',
      Connection: 'keep-alive',
      'accept-encoding': 'gzip, deflate',
      Host: 'portal.paychekplus.com',
      'Cache-Control': 'no-cache',
      'Content-Type': 'text/xml',
      Accept: '*/*' },
    
      body: 
      '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:fsv="http://FSVWebServices/FSVRemote/">\n'    
      +'<soapenv:Header/>\n'    
      +'<soapenv:Body>\n'        
      +'<fsv:Transact>\n'
      +'<fsv:userid>PAYRIGHT</fsv:userid>\n'
      +'<fsv:password>IrF=PvUzY3!S2gMaYA</fsv:password>\n'
      + '<fsv:paramstr>\n'                
      +'<![CDATA[' 
      + cdata +']]>\n'            
      +'\n'            
      +'</fsv:paramstr>\n'
      +'</fsv:Transact>\n'
      +'</soapenv:Body>\n'
      +'</soapenv:Envelope>' };   

        request(options, function (error, response, body) {
          if (error) throw new Error(error);
    
          //console.log(body)
    
          //Get Employee ID from the successful response
          var b4Body = body.replace ('<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><TransactResponse xmlns="http://FSVWebServices/FSVRemote/"><TransactResult>',"")
          var aBody = b4Body.replace('</TransactResult></TransactResponse></soap:Body></soap:Envelope>',"")
          var empCardId = aBody.slice(42,51)
          var empId = aBody.slice(53,62)
          console.log(body)
          
          //Necessary variables for put request
          var fsvTable = cdata.split(",")
          var chEmployeeId = fsvTable[37];
          var chLastName = fsvTable[6];
          var chFirstName = fsvTable[7];
          var userName = fsvTable[21]; 
          var nhReserved9 = fsvTable[44];
          var nhReserved10 = fsvTable[45];
          var chLocationId = fsvTable[31];
          //var EmpId = fsvTable[21]//Will be the empID gotten from the response in the main program. 
          var todaysDate = moment().format("YYYY-MM-DD");
          var emptyStatus = ""//used for testing purposes. 
          var status = "API Transfer"
    
          //Get position of Employee in the CSV table. 
          var position = index + 1;
    
          //Log Successful Responses. 
          var successMessage = position +"." + " Employee with id "+chFirstName+ " " +chLastName+ " succesfully registered."
          //console.log(successMessage)
    
          //Default Error Message
          var errorMessage = 
          "<h3>"+"There is an error at row "+position+" in cdata.csv. "+"</h3>"
          +"<p>"+"Please check your kronos records for " +"<b>"+chFirstName+" "+chLastName+"</b></p>"
          +"<p>"+"Here is the response from FSV: "+"</p>"
          + "<p><b>"+body+"</b></p>"
          +"<p>"+"<----------------------------------------------------------------------------------------------------------------------->"+"</p>";

          var updateStatus = 
          "<p>"+"Please check your kronos records for " +"<b>"+chFirstName+" "+chLastName+"</b></p>"
          +"<p>"+"Here is the response from FSV: "+"</p>"
          + "<p><b>"+body+"</b></p>"
          +"<p>"+"<----------------------------------------------------------------------------------------------------------------------->"+"</p>";

          var unknownError = 
          "<h3>"+"There is an unknown error at row "+position+" in cdata.csv."+"</h3>"
          +"<p>"+"Please check your kronos records for " +"<b>"+chFirstName+" "+chLastName+"</b></p>"
          +"<p>"+"Here is the response from FSV: "+"</p>"
          + "<p><b>"+body+"</b></p>"
          +"<p>"+"<----------------------------------------------------------------------------------------------------------------------->"+"</p>";

          //console.log(empID +" and "+empCardID)
          //If the response contains any string that says invalid, send an email response to Doug and App Dev. 
          if (body.includes("java.lang.IllegalArgumentException") !== true){
            if (body.includes("Invalid") == true || ("invalid") == true) {
              sendMessage.mail(errorMessage)
              //sendMessage.mail()
              log.info(errorMessage)
            }
            else if(body.includes("not allowed") == true){
              console.log("There is an error at row "+position+" in cdata.csv. " + "Please check your kronos records: "+body);
              sendMessage.mail(errorMessage)
              log.info(errorMessage)            
            } 
            else if (body.includes("successful") == true && chEmployeeId != null){
              
              if (chEmployeeId.length > 0){
  
                console.log(successMessage)
                //successMail.successMail(updateStatus, chEmployeeId)
                //console.log(empId +" and "+empCardId)
               async function generatePreNote(){
                 PNImport.PNImport(chEmployeeId,nhReserved9,nhReserved10)
                }

                  function apiTransUpdate(){
                      Promise.all([
                        //PNImport.PNImport(),
                        getEmpRequest.getEmpRequest(chEmployeeId), 
                        successMail.successMail(updateStatus, chEmployeeId)])
                      //console.log(data); 
                }

              async function kronosImport(){

                await generatePreNote()
                apiTransUpdate()

              }  
              kronosImport()

                // getPreNote.getPreNote(chEmployeeId)
                //   setTimeout(function () {
                // PNImport.PNImport()      
                // }, 2000); 

                // setTimeout(function () {
                // Promise.all([
                //   getEmpRequest.getEmpRequest(chEmployeeId), 
                //   successMail.successMail(updateStatus, chEmployeeId)])      
                //   }, 5000);
                //getPreNote.getPreNote(chEmployeeId)             


              }
            }
            else if (body.includes("faultstring")){
              console.log("Undefined Error")
            }
            else if (body.includes("Undefined Error")){
              console.log("Undefined Error")
            }
            else if(body.includes("HTTP Status 404")) {
              log.info(errorMessage)              
              console.log("There was an error connecting for Employee "+chEmployeeId);
              sendMessage.mail("There was an error connecting to FSV for Employee "+chFirstName + " "+chLastName+ " with ID: " +chEmployeeId)+". FSV is returning a 404 error, contact them for more infromation."

            }
            else{
              sendMessage.mail(unknownError)
      
            }

          }
          else {
            console.log("No value, probably an extra row")
            console.log(chEmployeeId)
          }  
        });
    });
    
    //delete errors.html file after 1500 miliseconds
    function sleep (time) {
      return new Promise((resolve) => setTimeout(resolve, time));
    }
    sleep(15000).then(() => {
      // Do something after the sleep!
    //   fs.unlink('error_log/errors2.html', function (err) {
    //     if (err) throw err;
    //     // if no error, file has been deleted successfully
    //     console.log('Errors.html file deleted!');
    // }); 
  //   fs.unlink('cdata.csv', function (err) {
  //     if (err) throw err;
  //     // if no error, file has been deleted successfully
  //     console.log('cdata.csv file deleted!');
  // }); 

  })

