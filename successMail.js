const nodemailer = require("nodemailer");


exports.successMail =function successMail(errorMessage, chEmployeeId){  
 async function main(){

  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let testAccount = await nodemailer.createTestAccount();

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: "fsv.hendel.payright@gmail.com", // generated ethereal user
      pass: "ChangeMe$2", // generated ethereal password
    }
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: 'alerts@payrightpayroll.com', // sender address
    to: "yomi@payrightpayroll.com, ddowns@payrightpayroll.com, devina@payrightpayroll.com, rle@payrightpayroll.com, asilverman@payrightpayroll.com", // list of receivers: ddowns@payrightpayroll.com, appdev@payrightpayroll.com
    subject: "Hendel FSV - Registration Alert", // Subject line
    text: errorMessage, // plain text body
    html: errorMessage, // html body
    attachments: [
      {   // utf-8 string as an attachment
          filename: 'cdata.csv',
          path: "./cdata.csv",
      },



],

  });

  //console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  //console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...\
  return errorMessage
  
}
main().catch(console.error); 

}
