var request = require("request");
var currToken = require('./getToken');
var fs = require('fs');
var jsonexport = require('jsonexport');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
var importFSV = require("./import")
var api_transfer_mail = require("./error_emails/api_transfer_email")


exports.getEmpRequest = function getEmpRequest(chEmployeeId){
currToken.activeToken(function(er, token) {
    if (er) {
      api_transfer_mail.api_transfer_mail(er)
      return console.log(er);
    };
  
    var options = { method: 'GET',
    url: 'https://secure.saashr.com/ta/rest/v2/companies/83927296/employees/'+chEmployeeId,
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authentication': 'Bearer '+token
    }  };

    request(options, function (error, response, body) {
      if (error) {
        api_transfer_mail.api_transfer_mail(error)
        console.log(error)
      }

    //console.log(JSON.parse(body));

    //FSV import function: parse JSON Body, creates excel file, and imports employee's data into Kronos. 
    async function FSVimport(){
        var jsonFile = JSON.parse(body)
   
            var employee_id = jsonFile.employee_id
            var file = "imports/"+employee_id+"_"+'import.csv'

            const csvWriter = createCsvWriter({
                path: file,
                header: [
                  {id: 'username', title: 'Username'},
                  {id: 'employee_id', title: 'Employee  Id'},
                  {id: 'first_name', title: 'First Name'},
                  {id: 'last_name', title: 'Last Name'},    
                  {id: 'email', title: "Primary Email"},       
                  {id: 'hired', title: 'Hired'},
                  {id: 'started', title: 'Started'},
                  {id: 'account_extra_1', title: 'Account Extra 1'},                                                                                                                                       
                ]
              });

              const data = [
                {
                  username: jsonFile.username,
                  employee_id: jsonFile.employee_id,
                  first_name: jsonFile.first_name,
                  last_name: jsonFile.last_name,
                  email: jsonFile.primary_email,
                  hired: jsonFile.dates.hired,
                  started: jsonFile.dates.started,
                  account_extra_1: "API Transfer"}];

                  csvWriter
                  .writeRecords(data)
                  .then(()=> importFSV.importFSV(file), 
                  console.log('The FSV CSV file was written successfully'),
                  );              
    


    }
    FSVimport()

        });
    })
}