var request = require("request");
var currToken = require('./getToken');
var fs = require('fs');
var jsonexport = require('jsonexport');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
var importPreNote = require("./preNoteImport")
const pre_note_mail = require("./error_emails/pre_note_error_email")

exports.getPreNote = function getPreNote(chEmployeeId){
currToken.activeToken(function(er, token) {
    if (er) {
      return console.log(er);
    };
  
    var options = { method: 'GET',
    url: 'https://secure.saashr.com/ta/rest/v2/companies/83927296/employees/'+chEmployeeId, 
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authentication': 'Bearer '+token
    }  };

    request(options, function (error, response, body) {
      if (error) {
        pre_note_mail.pre_note_mail("For employee with account: "+ chEmployeeId +" This is the error response: "+error)
        console.log(error)
      }
      else if (body == {}){
        pre_note_mail.pre_note_mail("For employee with account: "+ chEmployeeId +" This is the error response: "+error)
  
      };

    //var employee_id = jsonFile.employee_id
    //var username = jsonFile.username
      var jsonFile = JSON.parse(body)
      console.log("This is the body: "+body)
      
    let jsonEmployee = {
      account_id : chEmployeeId,
      employee_id : jsonFile.employee_id,
      username : jsonFile.username
    }

    let data = JSON.stringify(jsonEmployee)
    fs.writeFileSync('employee.json', data);

        });
    })
}