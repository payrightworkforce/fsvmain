//url: 'https://secure.saashr.com/ta/rest/v2/companies/83927296/employees/'+account_id+"/direct-deposits",


const fs = require('fs');
const currToken = require('./getToken');
const request = require("request");
var jsonexport = require('jsonexport');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
var importPreNote = require("./preNoteImport")
const pre_note_mail = require("./error_emails/pre_note_error_email")

//Need account_id, employee id and username
// account_id = chemployee_id from cdata.csv,
//username can be declared in index.js and employee_id can be replaced nreserved9


exports.PNImport = function PNImport(account_id,username,employee_id){

    // let rawdata = fs.readFileSync('employee.json');
    // if (rawdata == "{}"){
    //     console.log("The is an error here, do not proceed")
    //     pre_note_mail.pre_note_mail("There was an error while creating an import file for canceling the pre-note. Run FSV manually for more details")
    // }
    
        // let jsonFile = JSON.parse(rawdata);

        // var account_id = jsonFile.account_id
        // var employee_id = jsonFile.employee_id
        // var username = jsonFile.username


        function generatePNImport(){
            currToken.activeToken(function(er, token) {
                if (er) {
                return console.log(er);
                };
                //async function PreNoteimport(){
                    //console.log(jsonFile)
                    //console.log("This is the body "+body)


                var options = { method: 'GET',
                url: 'https://secure.saashr.com/ta/rest/v2/companies/83927296/employees/'+account_id+"/direct-deposits",
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authentication': 'Bearer '+token
                } };
                
                request(options, function (error, response, body) {
                    if (error) {
                        pre_note_mail.pre_note_mail(error)
                        console.log("This is the error: "+ error)
                    };
                

                        //console.log(body);
                        var jsonFile2 = JSON.parse(body)
                        var depositsLength = jsonFile2.deposits.length
                        console.log("Current pre note status: "+jsonFile2.deposits[0].pre_note_status)
                        //var index = depositsLength - 1
                        //console.log(jsonFile2)
                        //console.log(body2)

                        // if (jsonFile2.deposits[index].sequence == undefined){
                        //     pre_note_mail.pre_note_mail(error)
                        // }
                        
                        for (index = 0; index < jsonFile2.deposits.length; index++){
                            //var index = 0
                            //console.log("This is direct deposit loop: "+index)
                            //Check for "SEN" to detect SENT and READY TO SEND statuses.
                            if (jsonFile2.deposits[index].pre_note_status.includes("SEN")){
                                var pre_note_status = "Cancelled"
                                var sequence = jsonFile2.deposits[index].sequence
                                var frequency1 = jsonFile2.deposits[index].payroll_frequency.type.replace("_","")
                                var frequencyEvery = "EveryPay" 
                                var accountType = jsonFile2.deposits[index].account_type
                                var aba = jsonFile2.deposits[index].routing_number
                                var calc_method = jsonFile2.deposits[index].calc_method.display_name
                                var start_date = jsonFile2.deposits[index].active_from
                                var account_number = jsonFile2.deposits[index].account_number
                                //var einClient = "Trinity Mgmt LLC"
                                var file = "preNoteImports/"+account_id+"_"+'import.csv'

                                const csvWriter = createCsvWriter({
                                path: file,
                                header: [
                                    {id: 'username', title: 'Username'},
                                    {id: 'employee_id', title: 'Employee Id'},
                                    {id: 'pre_note_status', title: 'Pre-Note Status'},
                                    {id: 'sequence', title: 'Sequence'},
                                    {id: 'frequency', title: 'Frequency'},
                                    {id: 'accountType', title: 'Account Type'},
                                    {id: 'aba', title: 'ABA'},
                                    {id: 'calc_method', title: 'Calculation Method'},
                                    {id: 'start_date', title: 'Start Date'},
                                    {id: 'account_number', title: 'Account Number'},
                                    //{id: 'einName', title: 'EIN Name'},

                                ]
                                });

                                const data = [
                                {
                                    username: username,
                                    employee_id: employee_id,
                                    pre_note_status: "Cancelled", 
                                    sequence : jsonFile2.deposits[index].sequence,
                                    frequency : frequencyEvery,
                                    accountType : jsonFile2.deposits[index].account_type,
                                    aba : jsonFile2.deposits[index].routing_number,
                                    calc_method: jsonFile2.deposits[index].calc_method.display_name,
                                    start_date : jsonFile2.deposits[index].active_from,
                                    account_number: jsonFile2.deposits[index].account_number,
                                    //einName: einClient
                                }];

                                    csvWriter
                                    .writeRecords(data)
                                    .then(()=> importPreNote.importPreNote(file), 
                                    console.log('The Pre-Note CSV file was written successfully'),
                                    );   


                                //cancelPreNote.cancelPreNote(dd_id, abaNum)
                                //console.log("Here is the response from your put request "+response.statusMessage)
                            }
                        }
                    })

                //}
                
                //PreNoteimport(); 

            });
        }

        generatePNImport()
    

}