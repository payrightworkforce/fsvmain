const fs = require("fs")
const moment = require("moment")
const mv = require('mv');
var importPreNote = require("./preNoteImport")


//Today's date with moment.js - format XX/XX/XXXX
todaysDate = moment().format("L")
//console.log("Todays date is: "+todaysDate)

//Main Directory followed by directory where imported files are stored.
const folder = "C:\\Users\\ooyekunle\\Documents\\Github\\fsvHendel\\preNoteImports"
importsFolder = "C:\\Users\\ooyekunle\\Documents\\Github\\fsvHendel\\preNoteImports\\imported"

//Read the content of the directory, should only include pre-note imports and the imported folder
const filenames =  fs.readdirSync(folder)
filenames.forEach(file => {
  //Defining the path for the move 
    path = folder+"\\"+file 
    newPath = importsFolder+"\\"+file

  //Find the date modified for each file and reformat from timestamp (statSync returns timestamp)
  const stat = fs.statSync(path)
  modifiedDate = moment(stat.mtime).format("L")

//If the date modified is today's date, run the pre-note import
  if (modifiedDate == todaysDate){
    if (path !== "C:\\Users\\ooyekunle\\Documents\\Github\\fsvHendel\\preNoteImports\\imported"){
        console.log(path+ ": This file should be imported")
        //importPreNote.importPreNote(path)
    }
  }
  //Move file to imported directory
  else{
    console.log(path+ ": This file should be archived")
    async function moveFile(){

      mv(path, newPath, function(err) {
          // done. it tried fs.rename first, and then falls back to
          // piping the source file to the dest file and then unlinking
          // the source file.
      });
  }
  moveFile()
 }

});