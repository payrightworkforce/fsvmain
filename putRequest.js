//CDATA string: the total amount of parameters are 47.
//The first two parameters are required (this isn't necessarily clear in the documentation from FSV): the first is the Cert Card ID and the second is the Cert Card passcode. 
//The post request responds to every parameter after and including 3123, which is the transaction id. If you get an 
//error about the input record it applies to every value starting from 3123. 
//Refer to csv or txt file to see actual data. 

var request = require("request"); 
var fs = require("fs"); 
const nodemailer = require("nodemailer");
var sendMessage = require("./mail")
var moment = require("moment");
var currToken = require('./getToken')
var cancelPreNote = require("./cancelPreNote")


//console.log(chEmployeeId,chLastName,chFirstName,userName,nhReserved9, nhReserved10,chLocationId,todaysDate)
exports.sendPushRequest = function sendPushRequest(chEmployeeId){      
      currToken.activeToken(function(er, token) {
        if (er) {
          return console.log(er);
        };     
      

          var options = { method: 'GET',
                url: 'https://secure.saashr.com/ta/rest/v2/companies/83927296/employees/'+chEmployeeId+'/direct-deposits',
                headers: 
                { 'cache-control': 'no-cache',
                  Connection: 'keep-alive',
                  'Accept-Encoding': 'gzip, deflate',
                  Host: 'secure.saashr.com',
                  'Cache-Control': 'no-cache',
                  'User-Agent': 'PostmanRuntime/7.18.0',
                  Authentication: 'Bearer '+token,
                  Accept: 'application/json',
                  'Content-Type': 'application/json' } };

              request(options, function (error, response, body) {
                if (error) throw new Error(error);

                //console.log(body);
                var jsonFile = JSON.parse(body)

                var dd_id = jsonFile.deposits[0].id
                var abaNum = jsonFile.deposits[0].routing_number
                console.log("The direct deposit number is "+dd_id+" and the routing number is: "+abaNum)
                cancelPreNote.cancelPreNote(dd_id, abaNum)
                //console.log("Here is the response from your put request "+response.statusMessage)
              })

          });

}